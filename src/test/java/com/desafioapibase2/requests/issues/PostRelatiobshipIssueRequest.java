package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostRelatiobshipIssueRequest extends RequestRestBase {

    public PostRelatiobshipIssueRequest(int issuetId) {
        requestService = "/issues/" + issuetId + "/relationships";
        method = Method.POST;
    }

    public void setJsonBodyRelatiobshipIssues(int issueId, String issueTypeName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> JSONIssue = new HashMap<String, Object>();
        Map<String, Object> JSONType = new HashMap<String, Object>();


        JSONIssue.put("id", issueId);
        JSON.put("issue", JSONIssue);
        JSONType.put("name", issueTypeName);
        JSON.put("type", JSONType);

        jsonBody = JSON;
    }
}
