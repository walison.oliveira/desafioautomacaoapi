package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetIssuesRequest extends RequestRestBase {

    public GetIssuesRequest(int issuesId){
        requestService = "/issues/"+issuesId;
        method = Method.GET;
    }
}
