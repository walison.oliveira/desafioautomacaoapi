package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostIssuesRequest extends RequestRestBase {

    public PostIssuesRequest() {
        requestService = "/issues/";
        method = Method.POST;
    }

    public void setJsonBodyIssues(String summary, String description, String additional_information, int projectId, String projectName, int categoryId, String categoryName,
                                  String handlerName, int view_stateId, String view_stateName, String priorityName, String severityName, String reproducibilityName,
                                  boolean sticky, int custom_fieldsFieldId, String custom_fieldsFieldName, String custom_fieldsValue, String tagsName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> JSONProject = new HashMap<String, Object>();
        Map<String, Object> JSONCategory = new HashMap<String, Object>();
        Map<String, Object> JSONHandler = new HashMap<String, Object>();
        Map<String, Object> JSONViewState = new HashMap<String, Object>();
        Map<String, Object> JSONPriority = new HashMap<String, Object>();
        Map<String, Object> JSONSeverity = new HashMap<String, Object>();
        Map<String, Object> JSONReproducibility = new HashMap<String, Object>();
        Map<String, Object> JSONCustomField = new HashMap<String, Object>();
        Map<String, Object> JSONFields = new HashMap<String, Object>();
        Map<String, Object> JSONTags = new HashMap<String, Object>();


        JSON.put("summary", summary);
        JSON.put("description", description);
        JSON.put("additional_information", additional_information);
        JSONProject.put("id", projectId);
        JSONProject.put("name", projectName);
        JSON.put("project", JSONProject);
        JSONCategory.put("id", categoryId);
        JSONCategory.put("name", categoryName);
        JSON.put("category", JSONCategory);
        JSONHandler.put("name", handlerName);
        JSON.put("handler", JSONHandler);
        JSONViewState.put("id", view_stateId);
        JSONViewState.put("name", view_stateName);
        JSON.put("view_state", JSONViewState);
        JSONPriority.put("name", priorityName);
        JSON.put("priority", JSONPriority);
        JSONSeverity.put("name", severityName);
        JSON.put("severity", JSONSeverity);
        JSONReproducibility.put("name", reproducibilityName);
        JSON.put("reproducibility", JSONReproducibility);
        JSON.put("sticky", sticky);
        JSONFields.put("id", custom_fieldsFieldId);
        JSONFields.put("name", custom_fieldsFieldName);
        JSONCustomField.put("field", JSONFields);
        JSONCustomField.put("value", custom_fieldsValue);
        JSON.put("custom_fields",new Object[]{JSONCustomField});
        JSONTags.put("name", tagsName);
        JSON.put("tags", new Object[]{JSONTags});

        jsonBody = JSON;
    }

    public void setJsonBodyIssues(String summary, String description, String categoryName, String projectName) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> JSONProject = new HashMap<String, Object>();
        Map<String, Object> JSONCategory = new HashMap<String, Object>();

        JSON.put("summary", summary);
        JSON.put("description", description);
        JSONCategory.put("name", categoryName);
        JSON.put("category", JSONCategory);
        JSONProject.put("name", projectName);
        JSON.put("project", JSONProject);

        jsonBody = JSON;
    }
}
