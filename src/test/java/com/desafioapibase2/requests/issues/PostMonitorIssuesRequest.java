package com.desafioapibase2.requests.issues;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostMonitorIssuesRequest extends RequestRestBase {

    public PostMonitorIssuesRequest(int issuetId) {
        requestService = "/issues/" + issuetId + "/monitors";
        method = Method.POST;
    }

    public void setJsonBodyMonitorIssues(String usersName, String usersNameOrRealName) {

        Map<String, Object> JSONMonitorName = new HashMap<String, Object>();
        Map<String, Object> JSONMonitorRealName = new HashMap<String, Object>();
        Map<String, Object> JSON = new HashMap<String, Object>();

        JSONMonitorName.put("name", usersName);
        JSONMonitorRealName.put("name_or_realname", usersNameOrRealName);
        JSON.put("users", new Object[]{JSONMonitorName, JSONMonitorRealName});

        jsonBody = JSON;
    }
}