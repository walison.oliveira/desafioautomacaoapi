package com.desafioapibase2.requests.projects;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeleteProjectsRequest extends RequestRestBase {

    public DeleteProjectsRequest(int projetoId){
        requestService = "/projects/"+projetoId;
        method = Method.DELETE;
    }
}
