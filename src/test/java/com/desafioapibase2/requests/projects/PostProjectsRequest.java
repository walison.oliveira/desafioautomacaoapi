package com.desafioapibase2.requests.projects;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostProjectsRequest extends RequestRestBase {

    public PostProjectsRequest() {
        requestService = "/projects/";
        method = Method.POST;
    }

    public void setJsonBodProjects(String name, int StatusID, String StatusName, String StatusLabel, String description, boolean enabled, String file_path,
                                   int view_stateId, String view_stateName, String view_stateLabel) {

        Map<String, Object> JSONBody = new HashMap<String, Object>();

        JSONBody.put("name", name);
        JSONBody.put("statusId", StatusID);
        JSONBody.put("statusName", StatusName);
        JSONBody.put("statusLabel", StatusLabel);
        JSONBody.put("description", description);
        JSONBody.put("enabled", enabled);
        JSONBody.put("file_path", file_path);
        JSONBody.put("view_stateId", view_stateId);
        JSONBody.put("view_stateName", view_stateName);
        JSONBody.put("view_stateLabel", view_stateLabel);

        jsonBody = JSONBody;
    }
}
