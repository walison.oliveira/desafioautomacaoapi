package com.desafioapibase2.requests.users;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

import java.util.HashMap;
import java.util.Map;

public class PostUsersRequest extends RequestRestBase {

    public PostUsersRequest() {
        requestService = "/users/";
        method = Method.POST;
    }

    public void setJsonBodyUsers(String userName, String password, String realName, String email, String access_levelName, boolean enabled, boolean protectedUser) {

        Map<String, Object> JSON = new HashMap<String, Object>();
        Map<String, Object> JSONAcceslevel = new HashMap<String, Object>();

        JSON.put("username", userName);
        JSON.put("password", password);
        JSON.put("real_name", realName);
        JSON.put("email", email);
        JSONAcceslevel.put("name", access_levelName);
        JSON.put("enabled", enabled);
        JSON.put("protected", protectedUser);
        JSON.put("access_level", JSONAcceslevel);

        jsonBody = JSON;
    }
}
