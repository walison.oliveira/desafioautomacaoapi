package com.desafioapibase2.requests.users;

import com.desafioapibase2.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetUsersRequest extends RequestRestBase {

    public GetUsersRequest(){
        requestService = "/users/me";
        method = Method.GET;
    }
}
