package com.desafioapibase2.dbsteps;

import com.desafioapibase2.utils.DBUtils;
import com.desafioapibase2.utils.GeneralUtils;

public class TarefaDBSteps {

    private static String queriesPath = "src/test/java/com/desafioapibase2/queries/tarefa/";

    public static void TarefaDB(){
        criarTarefaDBSteps();
        criarFiltroDBSteps();
    }

    public static void criarTarefaDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarTarefa.sql");
        DBUtils.executeUpdate(query);
    }

    public static void criarFiltroDBSteps(){
        String query = GeneralUtils.readFileToAString(queriesPath + "criarFiltro.sql");
        DBUtils.executeUpdate(query);
    }
}
