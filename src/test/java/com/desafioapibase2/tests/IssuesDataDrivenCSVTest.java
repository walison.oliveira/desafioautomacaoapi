package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.requests.issues.PostIssuesRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IssuesDataDrivenCSVTest extends TestBase {

    @DataProvider(name="dataIssuesSucessoCSVProvider")
    public Iterator<Object []> dataUsersFailProvider2(){
        return csvProvider("src/test/java/issuesSucesso.csv");
    }

    @Test(dataProvider = "dataIssuesSucessoCSVProvider", groups = {"issues"})
    public void inserirIssuesComSucesso(String[] issues) {
        PostIssuesRequest postIssuesRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String summary = issues[0];
        String description = issues[1];
        String additionalInformation = issues[2];
        int projectId = Integer.valueOf(issues[3]);
        String projectName = issues[4];
        int categoryId = Integer.valueOf(issues[5]);
        String categoryName = issues[6];
        String handlerName = issues[7];
        int viewStateId = Integer.valueOf(issues[8]);
        String viewStateName = issues[9];
        String priority = issues[10];
        String severity = issues[11];
        String reproducibilityName = issues[12];
        boolean sticky = Boolean.valueOf(issues[13]);
        int customFieldsFieldId = Integer.valueOf(issues[14]);
        String customFieldsFieldName = issues[15];
        String customFieldsValeu = issues[16];
        String tagsName = issues[17];
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postIssuesRequest = new PostIssuesRequest();
        postIssuesRequest.setJsonBodyIssues(summary, description, additionalInformation, projectId, projectName,
                categoryId, categoryName, handlerName, viewStateId, viewStateName, priority, severity, reproducibilityName,
                sticky, customFieldsFieldId, customFieldsFieldName, customFieldsValeu, tagsName);
        Response response = postIssuesRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("issue.summary"), summary);
        softAssert.assertEquals(response.jsonPath().get("issue.description"), description);
        softAssert.assertEquals(response.jsonPath().get("issue.project.name"), projectName);
        softAssert.assertAll();
    }

    //Método para leitura do arquivo .csv
    public Iterator<Object []> csvProvider(String csvNamePath) {
        String line = "";
        String cvsSplitBy = ";";
        List<Object[]> testCases = new ArrayList<>();
        String[] data = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }
}
