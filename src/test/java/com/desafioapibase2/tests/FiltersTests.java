package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.requests.filters.DeleteFilterRequest;
import com.desafioapibase2.requests.filters.GetFilterRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class FiltersTests extends TestBase {

    @Test(groups = {"filters"})
    public void buscarTodosOsFiltrosComSucesso() {
        GetFilterRequest getFilterRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getFilterRequest = new GetFilterRequest();
        Response response = getFilterRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
    }

    @Test(groups = {"filters", "contrato"})
    public void testeContratoBuscarTodosOsFiltrosComSucesso() {
        GetFilterRequest getFilterRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getFilterRequest = new GetFilterRequest();
        Response response = getFilterRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetAllFiltersComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
    }

    @Test(groups = {"filters"})
    public void buscarFiltroComSucesso() {
        GetFilterRequest getFilterRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int filtroId = 2;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getFilterRequest = new GetFilterRequest(filtroId);
        Response response = getFilterRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
    }

    @Test(groups = {"filters", "contrato"})
    public void testeContratoBuscarFiltroComSucesso() {
        GetFilterRequest getFilterRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int filtroId = 2;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getFilterRequest = new GetFilterRequest(filtroId);
        Response response = getFilterRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetFiltersComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
    }

    @Test(groups = {"filters"})
    public void deletarFiltroComSucesso() {
        DeleteFilterRequest deleteFilterRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int filtroId = 2;
        int statusCodeEsperado = HttpStatus.SC_NO_CONTENT;

        //Fluxo
        deleteFilterRequest = new DeleteFilterRequest(filtroId);
        Response response = deleteFilterRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
    }
}
