package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.requests.users.DeleteUsersRequest;
import com.desafioapibase2.requests.users.GetUsersRequest;
import com.desafioapibase2.requests.users.PostUsersRequest;
import com.desafioapibase2.utils.Utils;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class UsersTests extends TestBase {

    @Test(groups = {"users"})
    public void inserirUsuarioComSucesso() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("user.name"), userName);
        softAssert.assertEquals(response.jsonPath().get("user.real_name"), realName);
        softAssert.assertEquals(response.jsonPath().get("user.email"), email);
        softAssert.assertEquals(response.jsonPath().get("user.access_level.name"), access_levelName);
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoInserirUsuarioComSucesso() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostUserComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void inserirUsuarioSemInformarName() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "";
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Invalid username '" + userName + "'");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(805));
        softAssert.assertEquals(response.jsonPath().get("localized"), "The username is invalid. Usernames may only contain Latin letters, numbers, spaces, hyphens, dots, plus signs and underscores.");
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoInserirUsuarioSemInformarName() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "";
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostUserSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void inserirUsuarioComEmailinvalido() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "desafioautomacao";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Email '" + email + "' is invalid.");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(1200));
        softAssert.assertEquals(response.jsonPath().get("localized"), "Invalid e-mail address.");
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoInserirUsuarioComEmailinvalido() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "desafioautomacao_001";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostUserSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void inserirUsuarioSemInformarAccessLevelName() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("message"), "Invalid access level");
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(29));
        softAssert.assertEquals(response.jsonPath().get("localized"), "Invalid value for 'access_level'");
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoInserirUsuarioSemInformarAccessLevelName() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "userautomacaoapi"+Utils.randomString(2);
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostUserSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void deletarUsuarioComSucesso() {
        DeleteUsersRequest deleteUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int userId = 2;
        int statusCodeEsperado = HttpStatus.SC_NO_CONTENT;

        //Fluxo
        deleteUsersRequest = new DeleteUsersRequest(userId);
        Response response = deleteUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void buscarUsuarioComSucesso() {
        GetUsersRequest getUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getUsersRequest = new GetUsersRequest();
        Response response = getUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoBuscarUsuarioComSucesso() {
        GetUsersRequest getUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        getUsersRequest = new GetUsersRequest();
        Response response = getUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/GetUserComSucessoContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }

    @Test(groups = {"users"})
    public void inserirUsuarioJaCadastrado() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "administrator";
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Utilizando expressão regular
        String fonte = response.jsonPath().get("message"); //Obtendo a mensagem fonte.
        Pattern pattern = Pattern.compile(userName); //Obtendo uma instância Pattern recebendo uma String que é a expressão regular.
        Matcher matcher = pattern.matcher(fonte); //Através do método matcher passo a fonte de busca
        while (matcher.find())

        //Asserções
        softAssert.assertEquals(10, matcher.start()); //Assert com expressão regular
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("code"), Integer.valueOf(800));
        softAssert.assertEquals(response.jsonPath().get("localized"), "That username is already being used. Please go back and select another one.");
        softAssert.assertAll();
    }

    @Test(groups = {"users", "contrato"})
    public void testeContratoInserirUsuarioJaCadastrado() {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = "administrator";
        String password = Utils.randomString(6);
        String realName = "User Automacao API "+Utils.randomString(6);
        String email = "automacao_"+Utils.randomString(4)+"@base2.com.br";
        String access_levelName = "updater";
        boolean enabled = true;
        boolean protectedUsers = false;
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        response.then().assertThat().body(matchesJsonSchemaInClasspath("contratos/PostUserSemInformarCampoObrigatorioContrato.json"));
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }
}
