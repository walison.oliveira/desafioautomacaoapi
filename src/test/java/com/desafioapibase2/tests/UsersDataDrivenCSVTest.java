package com.desafioapibase2.tests;

import com.desafioapibase2.bases.TestBase;
import com.desafioapibase2.requests.users.PostUsersRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UsersDataDrivenCSVTest extends TestBase {

    @DataProvider(name="dataUsersSucessoCSVProvider")
    public Iterator<Object []> dataUserSucessoProvider(){
        return csvProvider("src/test/java/usersSucesso.csv");
    }

    @Test(dataProvider = "dataUsersSucessoCSVProvider", groups = {"users"})
    public void inserirUsuarioComSucesso(String[] users) {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = users[0];
        String password = users[1];
        String realName = users[2];
        String email = users[3];
        String access_levelName = users[4];
        boolean enabled = Boolean.valueOf(users[5]);
        boolean protectedUsers = Boolean.valueOf(users[6]);
        int statusCodeEsperado = HttpStatus.SC_CREATED;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertEquals(response.jsonPath().get("user.name"), userName);
        softAssert.assertEquals(response.jsonPath().get("user.real_name"), realName);
        softAssert.assertEquals(response.jsonPath().get("user.email"), email);
        softAssert.assertEquals(response.jsonPath().get("user.access_level.name"), access_levelName);
        softAssert.assertAll();
    }

    @DataProvider(name="dataUsersFailCSVProvider")
    public Iterator<Object []> dataUsersFailProvider2(){
        return csvProvider("src/test/java/usersFail.csv");
    }

    @Test(dataProvider = "dataUsersFailCSVProvider", groups = {"users"})
    public void inserirUsuarioComDadoInvalido(String[] users) {
        PostUsersRequest postUsersRequest;
        SoftAssert softAssert = new SoftAssert();

        //Parâmetros
        String userName = users[0];
        String password = users[1];
        String realName = users[2];
        String email = users[3];
        String access_levelName = users[4];
        boolean enabled = Boolean.valueOf(users[5]);
        boolean protectedUsers = Boolean.valueOf(users[6]);
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Fluxo
        postUsersRequest = new PostUsersRequest();
        postUsersRequest.setJsonBodyUsers(userName, password, realName, email, access_levelName, enabled, protectedUsers);
        Response response = postUsersRequest.executeRequest();

        //Asserções
        softAssert.assertEquals(response.statusCode(), statusCodeEsperado);
        softAssert.assertAll();
    }


    //Método para leitura do arquivo .csv
    public Iterator<Object []> csvProvider(String csvNamePath) {
        String line = "";
        String cvsSplitBy = ";";
        List<Object[]> testCases = new ArrayList<>();
        String[] data = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }
}
